# Komputer Store

This program is a solution to Task01 of Javascript at Experis Academy Denmark
This program is a small webstore that allows you to work for money, and buy laptops.
On the frontpage you're able to take a loan, choose a laptop to buy, 

## Installation
Clone this repository, by running the following git commands: 

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:AndreasKjelstrup/komputer-store.git
git add .
```

## Usage
Run the application, and start a LiveServer connection to be forwarded to the homepage of the application

### Maintainers
Andreas Kjelstrup

### License
Copyright 2020, Andreas Kjelstrup