import { MOCK_LAPTOPS } from './laptops.mock.js';

function App() {

    // DOM Elements
    const elLaptopsList = document.getElementById('laptop-list');
    this.workBtn = document.getElementById('workBtn');
    this.bankBtn = document.getElementById('bankBtn');
    this.getLoanBtn = document.getElementById('getLoanBtn');
    this.payLoanBtn = document.getElementById('payLoanBtn');
    this.buyLaptopBtn = document.getElementById('buyLaptopBtn');

    this.loanDiv = document.getElementById('loanDiv');
    this.payLoanDiv = document.getElementById('payLoanDiv');
    this.dropdownMenu = document.getElementById('laptop-list');
    this.laptopInfoDiv = document.getElementById('laptop-info-div');

    this.paymentHTML = document.getElementById('payment');
    this.balanceHTML = document.getElementById('balance');
    this.outgoingLoanHTML = document.getElementById('loanAmount');

    this.laptopName = document.getElementById('laptop-name');
    this.laptopPrice = document.getElementById('laptop-price');
    this.laptopDescription = document.getElementById('laptop-description');
    this.laptopImage = document.getElementById('laptop-picture');

    // Properties
    this.balance = 0;
    this.payment = 0;
    this.outgoingLoan = 0;
    this.hasALoan = false;
    this.amountToBePaid = 0;
    const laptops = [...MOCK_LAPTOPS]; 


    // Methods
    this.init = function() {
        
        this.workBtn.addEventListener('click', this.workBtnClick.bind(this));
        this.bankBtn.addEventListener('click', this.bankBtnClick.bind(this));
        this.getLoanBtn.addEventListener('click', this.getLoanBtnClick.bind(this));
        this.payLoanBtn.addEventListener('click', this.payLoanBtnClick.bind(this));
        this.dropdownMenu.addEventListener('change', this.renderSpecificLaptop.bind(this));
        this.buyLaptopBtn.addEventListener('click', this.buyLaptop.bind(this));

        this.render();
    }

    this.render = function() {

        this.updateVisuals();
        this.renderLaptopNames();
    }

    this.updateVisuals = function() {

            this.paymentHTML.innerHTML = this.payment + " Kr.";
            this.balanceHTML.innerHTML = this.balance + " Kr.";
            this.outgoingLoanHTML.innerHTML = this.outgoingLoan + " Kr.";
    }

    this.renderLaptopNames = function() {

        laptops.forEach(function(laptop) {

            const elLaptop = document.createElement('option');
            elLaptop.value = laptop.id;
            elLaptop.innerText = laptop.name;
            elLaptopsList.appendChild(elLaptop);
        });
    }

    this.renderSpecificLaptop = function() {

        const index = this.dropdownMenu.value;

        if(index == -1){

            this.laptopInfoDiv.style.visibility = "hidden";
        } else {

            this.laptopInfoDiv.style.visibility = "visible";
            const selectedLaptop = laptops.find(laptop => laptop.id == index);

            this.laptopName.innerText = selectedLaptop.name;  
            this.laptopPrice.innerText = selectedLaptop.price + " DKK";
            this.laptopDescription.innerText = selectedLaptop.description;
            this.laptopImage.src = selectedLaptop.image;
        }
    }

    this.buyLaptop = function() {

        const index = this.dropdownMenu.value;
        const selectedLaptop = laptops.find(laptop => laptop.id == index);

        this.laptopPrice = selectedLaptop.price;

        if(this.balance < this.laptopPrice) {

            alert('You dont have enough money to buy this laptop');

        } else {

            this.balance -= this.laptopPrice;
            alert('congratulations on buying this amazing laptop!');
            this.updateVisuals();
        }
    }

    this.workBtnClick = function() {

        this.payment = this.payment + 100;
        this.updateVisuals();
    }

    this.bankBtnClick = function() {

        if(this.hasALoan === true){

            this.amountToBePaid = (this.payment / 100) * 10;
            this.payment = (this.payment / 100) * 90;
            this.outgoingLoan -= this.amountToBePaid;
            this.balance += this.payment;

            if (this.outgoingLoan === 0){
                this.hasALoan = false;
            }
            
        } else {
            this.balance += this.payment;     
        }

        this.payment = 0;  

        this.checkLoanVisbility();
        this.updateVisuals();

    }

    this.getLoanBtnClick = function() {

        if (this.hasALoan === true) {
            this.checkLoanVisbility();

        } else {

            // Open prompt window when client presses the button
            let input = prompt("Enter the amount you wish to loan", 0);

            // Check if the input is a number, or if the input is 0. If it passes, open a new prompt.
            while(isNaN(input) || input == 0) {
                input = prompt("Please enter a valid number or a number higher than 0");
            }

            this.outgoingLoan = parseInt(input);
            this.balance += this.outgoingLoan;
   
            this.hasALoan = true;

            this.updateVisuals();
            this.checkLoanVisbility();
        }

    }

    this.payLoanBtnClick = function() {

        if(this.payment === 0){
            alert("You need to work before you can pay off your loan." + "\n" + "Get back to work, minion!");
        }

        if(this.payment > this.outgoingLoan){
            this.payment = (this.payment - this.outgoingLoan);
            this.outgoingLoan = 0;

        } else {
            this.outgoingLoan -= this.payment;
            this.payment = 0;
        }
    
        if(this.outgoingLoan === 0) {
            this.hasALoan = false;
        
        }

        this.checkLoanVisbility();
        this.updateVisuals();
    } 
    
    this.checkLoanVisbility = function() {

        if(this.hasALoan === false){
            this.loanDiv.style.visibility = "hidden";
            this.payLoanDiv.style.visibility = "hidden";

        } else {
            this.loanDiv.style.visibility = "visible";
            this.payLoanDiv.style.visibility = "visible";
        }
    }
}


const app = new App(MOCK_LAPTOPS);
app.init();