export const MOCK_LAPTOPS = [
    {
        id: 1,
        name: 'Dog laptop',
        price: 1500,
        description: 'Pretty cool laptop. Dog not included.',
        image: 'https://images.unsplash.com/photo-1589652717521-10c0d092dea9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80'
    },
    {
        id: 2,
        name: 'Poor mans Apple',
        price: 3000,
        description: 'Heavily underpriced laptop, considering the brand.',
        image: 'https://toppng.com/uploads/preview/mac-laptop-png-11552846440tutvgf0j1u.png'
    },    
    {
        id: 3,
        name: 'Expensive mans Apple',
        price: 17000,
        description: 'May or may not turn on, price is accurate, considering the brand.',
        image: 'https://p7.hiclipart.com/preview/766/926/497/laptop-computer-icons-laptop-vector.jpg'
    },    
    {
        id: 4,
        name:  'The laptop of everyones dreams',
        price: 199995,
        description: 'The Lamborghini of laptops. Made of Gold. The stylus definitely didnt cost 150000 DKK',
        image: 'https://assets.stickpng.com/images/588526fb6f293bbfae451a3a.png'
    }
];

